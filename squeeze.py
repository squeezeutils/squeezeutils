import urllib.request, urllib.parse, urllib.error

import pcl_expect.tcp

def decode(s):
    return urllib.parse.unquote(s)

def encode(s):
    return urllib.parse.quote(s)

def get_simple_info(cli, request):
    cli.send("%s ?\n" % request)
    x = pcl_expect.Controller()
    while x.loop():
        if x.re(cli, "%s (.+)\n" % request):
            return decode(cli.match.group(1))
        if x.re(cli, "%s\n" % request):
            return None

def get_list_info(cli, request, params, start=0, nr=100):
    cli.send("%s %d %d %s\n" % (request, start, nr, params))
    x = pcl_expect.Controller()
    raw = None
    while x.loop():
        if x.re(cli, "%s %d %d (.+)\n" % (request, start, nr)):
            raw = cli.match.group(1)
            break
        if x.re(cli, "%s\n" % request):
            return None
    if raw is None:
        return None
    items = {}
    for item in raw.split(" "):
        key, value = decode(item).split(":", 1)
        if key == "url":
            value = decode(value)
        items[key] = value
    return items

class Player(object):
    def __init__(self, cli, playerindex):
        self.__cli = cli
        self.__props = {"playerindex": playerindex}
        for prop in ["id", "name", "ip", "model", "displaytype"]:
            self.__props[prop] = get_simple_info(cli, "player %s %s" % (
                    prop, playerindex))

    def cli(self):
        return self.__cli

    def get_info(self, info):
        return get_simple_info(self.__cli, "%s %s" % (encode(self.id()), info))

    def get_list_info(self, request, params):
        return get_list_info(self.__cli, "%s %s" % (encode(self.id()), request),
                             params)

    def do_command(self, command, args=None):
        cmd = "%s %s" % (encode(self.id()), command)
        if args is None:
            to_send = cmd
            to_match = cmd + " *\n"
        else:
            args = encode(args)
            to_send = cmd + " " + args
            to_match = cmd + ".*\n"
        self.__cli.send(to_send + "\n")
        x = pcl_expect.Controller()
        while x.loop():
            if x.re(self.__cli, to_match):
                break

    def playerindex(self):
        return self.__props["playerindex"]

    def id(self):
        return self.__props["id"]

    def name(self):
        return self.__props["name"]

    def ip(self):
        return self.__props["ip"]

    def model(self):
        return self.__props["model"]

    def displaytype(self):
        return self.__props["displaytype"]

    def pretty(self):
        return ("%(playerindex)2d: %(name)s\n"
                "    %(ip)s %(model)s %(id)s\n") % self.__props

def get_player(cli=None):
    if cli is None:
        cli = pcl_expect.tcp.TcpClient(("localhost", 9090))

    n_players = int(get_simple_info(cli, "player count"))
    players = []
    for i in range(n_players):
        players.append(Player(cli, i))

    if len(players) == 1:
        return players[0]

    for p in players:
        print(p.pretty())

    if len(players):
        return players[-1]
    else:
        return None
